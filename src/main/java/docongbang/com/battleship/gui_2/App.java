package docongbang.com.battleship.gui_2;

import java.io.File;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.utils.DifficultyLevel;
import docongbang.com.battleship.gui_2.scenes.new_game.NewGameController;
import docongbang.com.battleship.gui_2.scenes.new_game.NewGameModel;
import docongbang.com.battleship.gui_2.scenes.new_game.NewGameView;
import docongbang.com.battleship.gui_2.scenes.play.game.controllers.GameController;
import docongbang.com.battleship.gui_2.scenes.play.game.models.GameModel;
import docongbang.com.battleship.gui_2.scenes.play.game.views.GameView;
import docongbang.com.battleship.gui_2.scenes.ship_arrangement.ShipArrangementController;
import docongbang.com.battleship.gui_2.scenes.ship_arrangement.ShipArrangementModel;
import docongbang.com.battleship.gui_2.scenes.ship_arrangement.ShipArrangementView;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.media.AudioClip;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class App extends Application {
	public static Image IMAGE_SUBMARINE;
	public static Image IMAGE_DESTROYER;
	public static Image IMAGE_BATTLESHIP;
	public static Image IMAGE_CARRIER;
	public static Image IMAGE_CRUISER;
	public static Image IMAGE_CIVIL;
	public static Image IMAGE_BOMB;
	public static Image IMAGE_BOMB_TRIGGERED;
	public static Image IMAGE_HEART;
	public static Image IMAGE_BURN;
	public static Image IMAGE_BULLET;
	public static Image IMAGE_ATTACK_SUCCESS;
	public static Image IMAGE_ATTACK_FAIL;
	public static Image IMAGE_ATTACK_HAS_SHIP;
	public static Image IMAGE_ATTACK_NO_SHIP;
	public static Image IMAGE_PLACED;
	public static Image IMAGE_NOT_PLACED;
	public static AudioClip SOUND_MISSLE_LAUNCH;
	public static AudioClip SOUND_BOMB_EXPLODED;
	public static AudioClip SOUND_FAILED;
	public static AudioClip SOUND_SUCCEEDED;
	public static AudioClip SOUND_ROCKET_LAUNCH;

	public final static int IMAGE_HEART_SIZE = 8;
	public final static int IMAGE_SHIP_SIZE = 25;
	public final static int CELL_SIZE = 55;
	public final static String HEADER_FONT = "Tahoma";
	public final static FontWeight HEADER_FONT_WEIGHT = FontWeight.NORMAL;
	public final static int HEADER_FONT_SIZE = 20;

	private static Game game;
	private static Stage primaryStage;

	/**
	 * In this scene, user can arrange his/her fleet on the grid
	 * 
	 * @return
	 */
	public static Scene getSceneShipArrangement() {
		game.placeShipsAuto();
		ShipArrangementModel model = new ShipArrangementModel(game.getPlayer1());
		ShipArrangementView view = new ShipArrangementView();
		new ShipArrangementController(model, view);
		return new Scene((Parent) view.getRoot());
	}

	/**
	 * The main scene of application.
	 * 
	 * @return
	 */
	public static Scene getScenePlay() {
		game.initBombAndSailingShip();
		GameModel model = new GameModel(game);
		GameView view = new GameView();
		new GameController(model, view);

		return new Scene((Parent) view.getRoot());
	}

	/**
	 * The scene for creating new game.
	 * 
	 * @return
	 */
	public static Scene getSceneNewGame() {
		NewGameModel model = new NewGameModel();
		NewGameView view = new NewGameView();
		new NewGameController(model, view);

		return new Scene(view.getRoot());
	}

	/**
	 * Create a game between computer and player.
	 * 
	 * @param playerName
	 * @param difficulty
	 */
	public static void setGame(String playerName, DifficultyLevel difficulty) {
		App.game = new Game(playerName, difficulty);
	}

	/**
	 * The primary stage of application
	 * 
	 * @return
	 */
	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	@Override
	public void start(final Stage primaryStage) throws Exception {
		IMAGE_SUBMARINE = new Image(getClass().getResource("/submarine.png").toString(), true);
		IMAGE_DESTROYER = new Image(getClass().getResource("/destroyer.png").toString(), true);
		IMAGE_BATTLESHIP = new Image(getClass().getResource("/battleship.jpg").toString(), true);
		IMAGE_CARRIER = new Image(getClass().getResource("/carrier.jpg").toString(), true);
		IMAGE_CRUISER = new Image(getClass().getResource("/cruiser.png").toString(), true);
		IMAGE_CIVIL = new Image(getClass().getResource("/civil.png").toString(), true);
		IMAGE_BOMB = new Image(getClass().getResource("/bomb.png").toString(), true);
		IMAGE_BOMB_TRIGGERED = new Image(getClass().getResource("/bomb-exploded.png").toString(), true);
		IMAGE_HEART = new Image(getClass().getResource("/heart.png").toString(), true);
		IMAGE_BURN = new Image(getClass().getResource("/fire.png").toString(), true);
		IMAGE_BULLET = new Image(getClass().getResource("/bullet.png").toString(), true);
		IMAGE_ATTACK_SUCCESS = new Image(getClass().getResource("/attack_success.png").toString(), true);
		IMAGE_ATTACK_FAIL = new Image(getClass().getResource("/attack_fail.png").toString(), true);
		IMAGE_ATTACK_HAS_SHIP = new Image(getClass().getResource("/has_ship.png").toString(), true);
		IMAGE_ATTACK_NO_SHIP = new Image(getClass().getResource("/not_has_ship.png").toString(), true);
		IMAGE_PLACED = new Image(getClass().getResource("/was_placed.png").toString(), true);
		IMAGE_NOT_PLACED = new Image(getClass().getResource("/not_yet_placed.png").toString(), true);
		SOUND_MISSLE_LAUNCH = new AudioClip(getClass().getResource("/missle.wav").toString());
		SOUND_BOMB_EXPLODED = new AudioClip(getClass().getResource("/Bomb.wav").toString());
		SOUND_FAILED = new AudioClip(getClass().getResource("/Fail.wav").toString());
		SOUND_SUCCEEDED = new AudioClip(getClass().getResource("/Success.wav").toString());
		SOUND_ROCKET_LAUNCH = new AudioClip(getClass().getResource("/Rocket.wav").toString());
		App.primaryStage = primaryStage;
		primaryStage.setTitle("Battleship by bangdc");
		primaryStage.setScene(getSceneNewGame());
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}