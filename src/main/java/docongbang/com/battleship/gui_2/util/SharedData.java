package docongbang.com.battleship.gui_2.util;

public class SharedData<T> {
	private T data;
	private boolean fresh;

	/**
	 * Create a generic shared data
	 * 
	 * @return
	 */
	public synchronized T getData() {
		fresh = false;
		return data;
	}

	/**
	 * Set shared data
	 * 
	 * @param data
	 */
	public synchronized void setData(T data) {
		this.data = data;
		fresh = true;
	}

	/**
	 * Get shared data
	 * 
	 * @return
	 */
	public boolean isFresh() {
		return fresh;
	}
}
