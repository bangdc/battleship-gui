package docongbang.com.battleship.gui_2.util;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MailBox<T> {
	private int size;
	private List<T> messages;
	private ReentrantLock mailboxLock;
	private Condition allowToRead, allowToWrite;

	/**
	 * Initialize the mailbox FIFO with size
	 * 
	 * @param size
	 */
	public MailBox(int size) {
		this.size = size;
		messages = new LinkedList<T>();
		mailboxLock = new ReentrantLock();
		allowToRead = mailboxLock.newCondition();
		allowToWrite = mailboxLock.newCondition();
	}

	/**
	 * Write the message in the end of queue
	 * 
	 * @param message
	 */
	public void writeMessage(T message) {
		mailboxLock.lock();
		try {
			while (messages.size() == size) {
				allowToWrite.await();
			}
			messages.add(message);
			allowToRead.signalAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			mailboxLock.unlock();
		}
	}

	/**
	 * Get the first message in queue
	 * 
	 * @return
	 */
	public T readMessage() {
		T message = null;
		try {
			mailboxLock.lock();
			while (messages.size() == 0) {
				allowToRead.await();
			}
			message = messages.remove(0);
			allowToWrite.signalAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			mailboxLock.unlock();
		}
		return message;
	}
}
