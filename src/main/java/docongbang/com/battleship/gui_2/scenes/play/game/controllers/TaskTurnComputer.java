package docongbang.com.battleship.gui_2.scenes.play.game.controllers;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.gui_2.scenes.play.game.models.GameModel;

public class TaskTurnComputer implements Runnable {

	private GameModel gameModel;
	private GameController gameController;

	public TaskTurnComputer(GameModel gameModel, GameController gameController) {
		this.gameModel = gameModel;
		this.gameController = gameController;
	}

	public void run() {
		// computer is intelligent. Just need a little amount of time to think
		// Computer can be simulated to miss the turn.
		int timeToThink = Game.TIME_CONSTRAINT_EACH_TURN / 20;
		try {
			Thread.sleep(timeToThink);
			gameModel.getTurnMissedSharedData().setData(false);
			gameController.getTimerThread().interrupt();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
