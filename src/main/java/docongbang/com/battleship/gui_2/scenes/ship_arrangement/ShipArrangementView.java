package docongbang.com.battleship.gui_2.scenes.ship_arrangement;

import java.util.Hashtable;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.naval.Battleship;
import docongbang.com.battleship.api.ship.naval.Carrier;
import docongbang.com.battleship.api.ship.naval.Cruiser;
import docongbang.com.battleship.api.ship.naval.Destroyer;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.ship.naval.Submarine;
import docongbang.com.battleship.api.utils.Helper;
import docongbang.com.battleship.api.utils.ShipOrientation;
import docongbang.com.battleship.gui_2.App;
import docongbang.com.battleship.gui_2.scenes.play.game.views.CellTrackingFleetView;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class ShipArrangementView {
	private BorderPane root;

	private GridPane gridPaneShips;
	private HBox center;

	private TilePane playerGridTrackingFleetView;

	private Hashtable<Integer, CellTrackingFleetView> playerTrackingFLeetCellViews;
	private Hashtable<NavalShip, ChoiceBox<ShipOrientation>> shipOrientationChoiceBoxes;
	private Hashtable<NavalShip, ChoiceBox<Integer>> shipOriginRowChoiceBoxes;
	private Hashtable<NavalShip, ChoiceBox<Character>> shipOriginColChoiceBoxes;
	private Hashtable<NavalShip, Button> shipClearButtons;
	private Hashtable<NavalShip, Button> shipUpdateButtons;
	private Hashtable<NavalShip, ImageView> shipPlacedStatusImages;

	private Alert alert;

	private ShipArrangementModel model;

	private Button btnReady;

	public ShipArrangementView() {
		root = new BorderPane();
		center = new HBox(5);

		playerGridTrackingFleetView = new TilePane();
		playerGridTrackingFleetView.setMinWidth(App.CELL_SIZE * (Game.GRID_WIDTH + 1));
		playerGridTrackingFleetView.setMinHeight(App.CELL_SIZE * (Game.GRID_HEIGHT + 1));
		playerGridTrackingFleetView.setPrefColumns(Game.GRID_WIDTH + 1);
		playerGridTrackingFleetView.setPrefRows(Game.GRID_HEIGHT + 1);

		playerTrackingFLeetCellViews = new Hashtable<Integer, CellTrackingFleetView>();

		HBox.setMargin(playerGridTrackingFleetView, new Insets(0, 0, 0, 5));

		gridPaneShips = new GridPane();
		gridPaneShips.setHgap(10);
		gridPaneShips.setVgap(10);
		gridPaneShips.setPadding(new Insets(25, 25, 25, 25));

		Text scenetitle = new Text("Ships Arrangement");
		scenetitle.setFont(Font.font(App.HEADER_FONT, App.HEADER_FONT_WEIGHT, App.HEADER_FONT_SIZE));
		gridPaneShips.add(scenetitle, 0, 0, 6, 1);

		int k = 0;
		gridPaneShips.add(new Text("Ship"), k++, 1);
		gridPaneShips.add(new Text("Width"), k++, 1);
		gridPaneShips.add(new Text("Length"), k++, 1);
		gridPaneShips.add(new Text("Column"), k++, 1);
		gridPaneShips.add(new Text("Row"), k++, 1);
		gridPaneShips.add(new Text("Orientation"), k++, 1);

		btnReady = new Button("Ready");
		gridPaneShips.add(btnReady, 0, 7, 6, 1);

		center.getChildren().addAll(gridPaneShips, playerGridTrackingFleetView);
		root.setCenter(center);

		shipOrientationChoiceBoxes = new Hashtable<NavalShip, ChoiceBox<ShipOrientation>>();
		shipOriginRowChoiceBoxes = new Hashtable<NavalShip, ChoiceBox<Integer>>();
		shipOriginColChoiceBoxes = new Hashtable<NavalShip, ChoiceBox<Character>>();
		shipClearButtons = new Hashtable<NavalShip, Button>();
		shipUpdateButtons = new Hashtable<NavalShip, Button>();
		shipPlacedStatusImages = new Hashtable<NavalShip, ImageView>();

		alert = new Alert(AlertType.INFORMATION);
	}

	public void init() {
		Player player = model.getPlayer();

		boolean allShipPlaced = true;
		for (int i = 0; i < player.getShips().size(); i++) {
			NavalShip navalShip = player.getShips().get(i);

			ChoiceBox<Character> choiceBoxCol = new ChoiceBox<Character>();
			choiceBoxCol.setItems(model.getColOptions());
			choiceBoxCol.getSelectionModel().select(Character.valueOf(navalShip.getOrigin().getCol()));

			ChoiceBox<Integer> choiceBoxRow = new ChoiceBox<Integer>();
			choiceBoxRow.setItems(model.getRowOptions());
			choiceBoxRow.getSelectionModel().select(Integer.valueOf(navalShip.getOrigin().getRow()));

			ChoiceBox<ShipOrientation> choiceBoxOrientation = new ChoiceBox<ShipOrientation>();
			choiceBoxOrientation.setItems(FXCollections.observableArrayList(ShipOrientation.values()));
			choiceBoxOrientation.getSelectionModel().select(navalShip.getOrientation());

			Button btnUpdate = new Button("Update");
			btnUpdate.setDisable(true);
			Button btnClear = new Button("Clear");

			ImageView imageViewPlacedStatus = new ImageView();
			imageViewPlacedStatus.setFitHeight(App.IMAGE_SHIP_SIZE);
			imageViewPlacedStatus.setPreserveRatio(true);

			if (navalShip.wasPlacedOnGrid()) {
				imageViewPlacedStatus.setImage(App.IMAGE_PLACED);
			} else {
				imageViewPlacedStatus.setImage(App.IMAGE_NOT_PLACED);
			}

			ImageView imageViewShip = new ImageView();
			imageViewShip.setFitHeight(App.IMAGE_SHIP_SIZE);
			imageViewShip.setPreserveRatio(true);

			if (navalShip instanceof Battleship) {
				imageViewShip.setImage(App.IMAGE_BATTLESHIP);
			}
			if (navalShip instanceof Carrier) {
				imageViewShip.setImage(App.IMAGE_CARRIER);
			}
			if (navalShip instanceof Cruiser) {
				imageViewShip.setImage(App.IMAGE_CRUISER);
			}
			if (navalShip instanceof Destroyer) {
				imageViewShip.setImage(App.IMAGE_DESTROYER);
			}
			if (navalShip instanceof Submarine) {
				imageViewShip.setImage(App.IMAGE_SUBMARINE);
			}

			allShipPlaced &= navalShip.wasPlacedOnGrid();

			shipPlacedStatusImages.put(navalShip, imageViewPlacedStatus);
			shipOriginColChoiceBoxes.put(navalShip, choiceBoxCol);
			shipOriginRowChoiceBoxes.put(navalShip, choiceBoxRow);
			shipOrientationChoiceBoxes.put(navalShip, choiceBoxOrientation);
			shipUpdateButtons.put(navalShip, btnUpdate);
			shipClearButtons.put(navalShip, btnClear);

			int k = 0;
			gridPaneShips.add(imageViewShip, k++, i + 2);
			gridPaneShips.add(new Text(navalShip.getWidth() + " cells"), k++, i + 2);
			gridPaneShips.add(new Text(navalShip.getLength() + " cells"), k++, i + 2);
			gridPaneShips.add(choiceBoxCol, k++, i + 2);
			gridPaneShips.add(choiceBoxRow, k++, i + 2);
			gridPaneShips.add(choiceBoxOrientation, k++, i + 2);
			gridPaneShips.add(btnUpdate, k++, i + 2);
			gridPaneShips.add(btnClear, k++, i + 2);
			gridPaneShips.add(imageViewPlacedStatus, k++, i + 2);
		}

		btnReady.setDisable(!allShipPlaced);

		Hashtable<Integer, CellTrackingFleet> playerTrackingFleetCells = player.getGridTrackingFleet().getCells();

		playerGridTrackingFleetView.getChildren().add(new Label());
		for (int col = 0; col < Game.GRID_WIDTH; col++) {
			playerGridTrackingFleetView.getChildren().add(new Label(String.valueOf((char) ('A' + col))));
		}
		for (int row = 0; row < Game.GRID_HEIGHT; row++) {
			playerGridTrackingFleetView.getChildren().add(new Label(String.valueOf(row + 1)));
			for (int col = 0; col < Game.GRID_WIDTH; col++) {
				Integer cellIndex = Integer.valueOf(Helper.calculatePairingNumber(col, row));

				CellTrackingFleetView playerCellTrackingFleetView = new CellTrackingFleetView(
						playerTrackingFleetCells.get(cellIndex));
				playerTrackingFLeetCellViews.put(cellIndex, playerCellTrackingFleetView);
				playerGridTrackingFleetView.getChildren().add(playerCellTrackingFleetView.getRoot());
			}
		}
	}

	public void update() {
		Player player = model.getPlayer();
		Hashtable<Integer, CellTrackingFleet> playerTrackingFleetCells = player.getGridTrackingFleet().getCells();

		for (int col = 0; col < Game.GRID_WIDTH; col++) {
			for (int row = 0; row < Game.GRID_HEIGHT; row++) {
				Integer cellIndex = Integer.valueOf(Helper.calculatePairingNumber(col, row));
				getplayerTrackingFLeetCellViews().get(cellIndex).update(playerTrackingFleetCells.get(cellIndex));
			}
		}

		boolean allShipPlaced = true;
		for (int i = 0; i < player.getShips().size(); i++) {
			NavalShip navalShip = player.getShips().get(i);

			if (navalShip.wasPlacedOnGrid()) {
				shipPlacedStatusImages.get(navalShip).setImage(App.IMAGE_PLACED);

				shipOriginColChoiceBoxes.get(navalShip).getSelectionModel()
						.select(Character.valueOf(navalShip.getOrigin().getCol()));
				shipOriginRowChoiceBoxes.get(navalShip).getSelectionModel()
						.select(Integer.valueOf(navalShip.getOrigin().getRow()));

				shipOrientationChoiceBoxes.get(navalShip).getSelectionModel().select(navalShip.getOrientation());
				shipUpdateButtons.get(navalShip).setDisable(true);
			} else {
				shipPlacedStatusImages.get(navalShip).setImage(App.IMAGE_NOT_PLACED);
				shipUpdateButtons.get(navalShip).setDisable(false);
			}
			allShipPlaced &= navalShip.wasPlacedOnGrid();
		}

		btnReady.setDisable(!allShipPlaced);
	}

	public Node getRoot() {
		return root;
	}

	public Hashtable<Integer, CellTrackingFleetView> getplayerTrackingFLeetCellViews() {
		return playerTrackingFLeetCellViews;
	}

	public void setModel(ShipArrangementModel model) {
		this.model = model;
	}

	public ShipArrangementModel getModel() {
		return model;
	}

	public Hashtable<NavalShip, ChoiceBox<ShipOrientation>> getShipOrientationChoiceBoxes() {
		return shipOrientationChoiceBoxes;
	}

	public Hashtable<NavalShip, ChoiceBox<Integer>> getShipOriginRowChoiceBoxes() {
		return shipOriginRowChoiceBoxes;
	}

	public Hashtable<NavalShip, ChoiceBox<Character>> getShipOriginColChoiceBoxes() {
		return shipOriginColChoiceBoxes;
	}

	public Hashtable<NavalShip, Button> getShipClearButtons() {
		return shipClearButtons;
	}

	public Hashtable<NavalShip, Button> getShipUpdateButtons() {
		return shipUpdateButtons;
	}

	public Alert getAlert() {
		return alert;
	}

	public Hashtable<NavalShip, ImageView> getShipPlacedStatusImages() {
		return shipPlacedStatusImages;
	}

	public Button getBtnReady() {
		return btnReady;
	}
}
