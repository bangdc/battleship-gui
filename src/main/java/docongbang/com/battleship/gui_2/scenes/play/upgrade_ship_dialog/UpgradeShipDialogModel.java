package docongbang.com.battleship.gui_2.scenes.play.upgrade_ship_dialog;

import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class UpgradeShipDialogModel {
	private Player player;
	private NavalShip ship;
	private CellTrackingFleet cell;

	public UpgradeShipDialogModel(Player player) {
		this.player = player;
		ship = player.getShips().get(0);
		cell = ship.getCells().get(0);
	}

	public Player getPlayer() {
		return player;
	}

	public CellTrackingFleet getCell() {
		return cell;
	}

	public NavalShip getShip() {
		return ship;
	}

	public void setShip(NavalShip ship) {
		this.ship = ship;
		this.cell = ship.getCells().get(0);
	}

	public void setCell(CellTrackingFleet cell) {
		this.cell = cell;
	}

	public ObservableList<NavalShip> getShipOptions() {
		return FXCollections.observableArrayList(player.getShips());
	}

	public ObservableList<CellTrackingFleet> getCellOptions() {
		return FXCollections.observableArrayList(ship.getCells());
	}
}
