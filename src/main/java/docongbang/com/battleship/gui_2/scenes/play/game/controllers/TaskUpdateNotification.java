package docongbang.com.battleship.gui_2.scenes.play.game.controllers;

import docongbang.com.battleship.gui_2.scenes.play.game.models.GameModel;
import docongbang.com.battleship.gui_2.scenes.play.game.views.GameView;
import javafx.application.Platform;

public class TaskUpdateNotification implements Runnable {
	private GameView gameView;
	private GameModel gameModel;

	public TaskUpdateNotification(GameModel gameModel, GameView gameView) {
		this.gameModel = gameModel;
		this.gameView = gameView;
	}

	public void run() {
		while (true) {
			final String newNotification = gameModel.getNotifications().readMessage();
			Platform.runLater(new Runnable() {

				public void run() {
					gameView.getNotificationsView().getNotifications().getItems().add(0, newNotification);
				}
			});

			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				break;
			}
		}

	}
}
