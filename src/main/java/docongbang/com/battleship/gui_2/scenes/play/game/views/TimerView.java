package docongbang.com.battleship.gui_2.scenes.play.game.views;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.gui_2.App;
import docongbang.com.battleship.gui_2.util.SharedData;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class TimerView {
	private VBox root;
	private ProgressBar timer;
	Task<Void> timerTask;

	public TimerView() {
		root = new VBox(10);
		root.setPadding(new Insets(25, 25, 25, 25));

		Text title = new Text("Timer");
		title.setFont(Font.font(App.HEADER_FONT, App.HEADER_FONT_WEIGHT, App.HEADER_FONT_SIZE));

		timer = new ProgressBar();
		timer.setMinWidth(200);

		root.getChildren().addAll(title, timer);
	}

	public VBox getRoot() {
		return root;
	}

	public ProgressBar getTimer() {
		return timer;
	}

	public void startTimerTask(final SharedData<Double> timerSharedData) {
		timerTask = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				while (true) {
					double progress = 0;
					Double theProgress = timerSharedData.getData();
					if (theProgress != null) {
						progress = theProgress.doubleValue();
					}
					updateProgress(progress, 100);
					Thread.sleep(Game.TIME_CONSTRAINT_EACH_TURN / 200);
				}
			}
		};
		timer.progressProperty().bind(timerTask.progressProperty());
		Thread timerTaskThread = new Thread(timerTask);
		timerTaskThread.setDaemon(true);
		timerTaskThread.start();
	}
}
