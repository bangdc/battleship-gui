package docongbang.com.battleship.gui_2.scenes.play.command_board;

import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.utils.AttackStrategy;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class CommandBoardController implements ChangeListener<Object> {
	private CommandBoardModel model;
	private CommandBoardView view;

	public CommandBoardController(CommandBoardModel model, CommandBoardView view) {
		this.model = model;
		this.view = view;

		view.setModel(model);
		view.update();

		view.getStrategyChoiceBox().getSelectionModel().selectedItemProperty().addListener(this);
		view.getShipChoiceBox().getSelectionModel().selectedItemProperty().addListener(this);
		view.getColChoiceBox().getSelectionModel().selectedItemProperty().addListener(this);
		view.getRowChoiceBox().getSelectionModel().selectedItemProperty().addListener(this);
	}

	public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
		if (newValue instanceof AttackStrategy) {
			AttackStrategy strategy = (AttackStrategy) newValue;
			if (strategy != model.getAttackStrategy()) {
				model.setAttackStrategy((AttackStrategy) newValue);
				view.update();
			}
			return;
		}
		if (newValue instanceof NavalShip) {
			NavalShip ship = (NavalShip) newValue;
			if (ship != model.getDutyShip()) {
				model.setDutyShip(ship);
				view.update();
			}
			return;
		}
		if (newValue instanceof Character) {
			Character col = (Character) newValue;
			if (col != model.getTargetCol()) {
				model.setTargetCol(col);
				view.update();
			}
			return;
		}
		if (newValue instanceof Integer) {
			Integer row = (Integer) newValue;
			if (row != model.getTargetRow()) {
				model.setTargetRow(row);
				view.update();
			}
			return;
		}
	}
}
