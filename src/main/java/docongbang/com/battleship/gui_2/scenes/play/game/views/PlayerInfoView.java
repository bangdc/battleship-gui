package docongbang.com.battleship.gui_2.scenes.play.game.views;

import java.util.ArrayList;
import java.util.List;

import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.naval.Battleship;
import docongbang.com.battleship.api.ship.naval.Carrier;
import docongbang.com.battleship.api.ship.naval.Cruiser;
import docongbang.com.battleship.api.ship.naval.Destroyer;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.ship.naval.Submarine;
import docongbang.com.battleship.gui_2.App;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class PlayerInfoView {
	private VBox root;
	private GridPane dispPlayerStats;
	private Text dispName;
	private Label dispTurnsLeft;
	private Label dispFailedTimes;
	private Label dispFailedTimesCons;
	private Label dispPlayedTimes;
	private Label dispSucceededTimes;
	private GridPane dispShips;
	private List<ShipHealthBarView> shipHealthBars;

	public PlayerInfoView(Player player) {
		root = new VBox();
		root.setAlignment(Pos.CENTER);

		dispName = new Text();
		dispName.setFont(Font.font(App.HEADER_FONT, App.HEADER_FONT_WEIGHT, App.HEADER_FONT_SIZE));
		dispTurnsLeft = new Label();
		dispFailedTimes = new Label();
		dispFailedTimesCons = new Label();
		dispPlayedTimes = new Label();
		dispSucceededTimes = new Label();
		dispShips = new GridPane();
		dispShips.setHgap(5);
		dispShips.setVgap(5);
		dispShips.setPadding(new Insets(15, 15, 15, 15));
		dispPlayerStats = new GridPane();
		dispPlayerStats.setHgap(5);
		dispPlayerStats.setVgap(5);
		dispPlayerStats.setPadding(new Insets(15, 15, 15, 15));

		root.getChildren().addAll(dispName, dispPlayerStats, dispTurnsLeft, dispPlayedTimes, dispSucceededTimes,
				dispFailedTimes, dispFailedTimesCons, dispShips);

		init(player);
	}

	private void init(Player player) {
		dispName.setText(player.getName());

		dispTurnsLeft.setText(String.valueOf(player.getNumberOfTurnsRemaining()));
		dispPlayerStats.add(new Label("You have: "), 0, 0);
		dispPlayerStats.add(dispTurnsLeft, 1, 0);
		dispPlayerStats.add(new Label(" turn(s)"), 2, 0);

		dispPlayedTimes.setText(String.valueOf(player.getNumberOfTurnsPlayed()));
		dispPlayerStats.add(new Label("You played: "), 0, 1);
		dispPlayerStats.add(dispPlayedTimes, 1, 1);
		dispPlayerStats.add(new Label(" turn(s)"), 2, 1);

		dispSucceededTimes.setText(String.valueOf(player.getNumberOfTurnsPlayed() - player.getNumberOfFailures()));
		dispPlayerStats.add(new Label("You succeeded: "), 0, 2);
		dispPlayerStats.add(dispSucceededTimes, 1, 2);
		dispPlayerStats.add(new Label(" turn(s)"), 2, 2);

		dispFailedTimes.setText(String.valueOf(player.getNumberOfFailures()));
		dispPlayerStats.add(new Label("You failed: "), 0, 3);
		dispPlayerStats.add(dispFailedTimes, 1, 3);
		dispPlayerStats.add(new Label(" turn(s)"), 2, 3);

		dispFailedTimesCons.setText(String.valueOf(player.getNumberOfConsecutiveFailures()));
		dispPlayerStats.add(new Label("You failed consecutively: "), 0, 4);
		dispPlayerStats.add(dispFailedTimesCons, 1, 4);
		dispPlayerStats.add(new Label(" turn(s)"), 2, 4);

		shipHealthBars = new ArrayList<ShipHealthBarView>();
		for (int i = 0; i < player.getShips().size(); i++) {
			NavalShip ship = player.getShips().get(i);
			Image image = null;
			if (ship instanceof Battleship) {
				image = App.IMAGE_BATTLESHIP;
			}
			if (ship instanceof Carrier) {
				image = App.IMAGE_CARRIER;
			}
			if (ship instanceof Cruiser) {
				image = App.IMAGE_CRUISER;
			}
			if (ship instanceof Destroyer) {
				image = App.IMAGE_DESTROYER;
			}
			if (ship instanceof Submarine) {
				image = App.IMAGE_SUBMARINE;
			}
			ImageView imageView = new ImageView(image);
			imageView.setFitWidth(App.IMAGE_SHIP_SIZE);
			imageView.setPreserveRatio(true);
			dispShips.add(new Label(ship.getName()), 0, i);
			dispShips.add(imageView, 1, i);

			ShipHealthBarView shipHealthBarView = new ShipHealthBarView(ship.getHealth(), ship.getHealth());
			dispShips.add(shipHealthBarView.getHealthProgressBar(), 2, i);
			shipHealthBars.add(shipHealthBarView);
		}
	}

	public void update(Player player) {
		dispTurnsLeft.setText(String.valueOf(player.getNumberOfTurnsRemaining()));
		dispPlayedTimes.setText(String.valueOf(player.getNumberOfTurnsPlayed()));
		dispSucceededTimes.setText(String.valueOf(player.getNumberOfTurnsPlayed() - player.getNumberOfFailures()));
		dispFailedTimes.setText(String.valueOf(player.getNumberOfFailures()));
		dispFailedTimesCons.setText(String.valueOf(player.getNumberOfConsecutiveFailures()));

		for (int i = 0; i < player.getShips().size(); i++) {
			NavalShip ship = player.getShips().get(i);
			shipHealthBars.get(i).update(ship.getHealth());
			shipHealthBars.get(i).getHealthProgressBar().setTooltip(new Tooltip(ship.getHealth() + " health(s)"));
		}
	}

	public VBox getRoot() {
		return root;
	}
}
