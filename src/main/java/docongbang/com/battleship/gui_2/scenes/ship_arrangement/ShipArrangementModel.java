package docongbang.com.battleship.gui_2.scenes.ship_arrangement;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.player.Player;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ShipArrangementModel {
	private Player player;
	private ObservableList<Integer> rowOptions;
	private ObservableList<Character> colOptions;

	public ShipArrangementModel(Player player) {
		this.player = player;
		rowOptions = FXCollections.observableArrayList();
		for (int i = 0; i < Game.GRID_HEIGHT; i++) {
			rowOptions.add(i + 1);
		}

		colOptions = FXCollections.observableArrayList();
		for (int i = 0; i < Game.GRID_WIDTH; i++) {
			colOptions.add(Character.valueOf((char) (i + (int) 'A')));
		}

	}

	public ObservableList<Character> getColOptions() {
		return colOptions;
	}

	public ObservableList<Integer> getRowOptions() {
		return rowOptions;
	}

	public Player getPlayer() {
		return player;
	}
}
