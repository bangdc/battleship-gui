package docongbang.com.battleship.gui_2.scenes.play.game.views;

import docongbang.com.battleship.api.grid.cell.CellBomb;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.ship.naval.Battleship;
import docongbang.com.battleship.api.ship.naval.Carrier;
import docongbang.com.battleship.api.ship.naval.Cruiser;
import docongbang.com.battleship.api.ship.naval.Destroyer;
import docongbang.com.battleship.api.ship.naval.Submarine;
import docongbang.com.battleship.gui_2.App;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class CellTrackingFleetView {
	private VBox root;
	private HBox main;
	private HBox cellHealthBar;

	private ImageView imageShipView;
	private ImageView imageBombView;

	public CellTrackingFleetView(CellTrackingFleet cell) {
		root = new VBox();
		main = new HBox(2);
		cellHealthBar = new HBox();

		root.setAlignment(Pos.CENTER);
		main.setAlignment(Pos.CENTER);
		cellHealthBar.setAlignment(Pos.CENTER);

		root.getChildren().addAll(cellHealthBar, main);

		imageShipView = new ImageView();
		imageShipView.setFitHeight(App.IMAGE_SHIP_SIZE);
		imageShipView.setPreserveRatio(true);

		imageBombView = new ImageView();
		imageBombView.setFitHeight(App.IMAGE_SHIP_SIZE);
		imageBombView.setPreserveRatio(true);

		main.getChildren().addAll(imageShipView, imageBombView);

		root.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

		root.setPrefHeight(App.CELL_SIZE);
		root.setPrefWidth(App.CELL_SIZE);

		update(cell);
	}

	public void update(CellTrackingFleet cell) {
		cellHealthBar.getChildren().clear();
		if (cell.doesHaveShip()) {
			if (cell.doesHaveCivilShip()) {
				imageShipView.setImage(App.IMAGE_CIVIL);
			}
			if (cell.doesHaveNavalShip()) {
				if (cell.getShip() instanceof Battleship) {
					imageShipView.setImage(App.IMAGE_BATTLESHIP);
				}
				if (cell.getShip() instanceof Carrier) {
					imageShipView.setImage(App.IMAGE_CARRIER);
				}
				if (cell.getShip() instanceof Cruiser) {
					imageShipView.setImage(App.IMAGE_CRUISER);
				}
				if (cell.getShip() instanceof Destroyer) {
					imageShipView.setImage(App.IMAGE_DESTROYER);
				}
				if (cell.getShip() instanceof Submarine) {
					imageShipView.setImage(App.IMAGE_SUBMARINE);
				}
			}
			cellHealthBar.getChildren().clear();
			if (cell.getHealth() > 0) {
				for (int i = 0; i < cell.getHealth(); i++) {
					ImageView imageView = new ImageView(App.IMAGE_HEART);
					imageView.setFitWidth(App.IMAGE_HEART_SIZE);
					imageView.setPreserveRatio(true);
					cellHealthBar.getChildren().add(imageView);
				}
			} else {
				imageShipView.setImage(App.IMAGE_BURN);
			}
		} else {
			imageShipView.setImage(null);
		}
		if (cell.doesHaveBomb()) {
			CellBomb cellBomb = (CellBomb) cell;
			if (cellBomb.wasAttacked()) {
				imageBombView.setImage(App.IMAGE_BOMB_TRIGGERED);
			} else {
				imageBombView.setImage(App.IMAGE_BOMB);
			}
		} else {
			imageBombView.setImage(null);
		}
		if (!cell.doesHaveBomb() && !cell.doesHaveShip() && cell.wasAttacked()) {
			imageShipView.setImage(App.IMAGE_BULLET);
		}
	}

	public VBox getRoot() {
		return root;
	}
}
