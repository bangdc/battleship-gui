package docongbang.com.battleship.gui_2.scenes.play.command_board;

import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.utils.AttackStrategy;
import docongbang.com.battleship.gui_2.App;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.StringConverter;

public class CommandBoardView {
	private GridPane root;
	private boolean disabled = true;
	private ChoiceBox<AttackStrategy> strategyChoiceBox;
	private ChoiceBox<NavalShip> shipChoiceBox;
	private ChoiceBox<Integer> rowChoiceBox;
	private ChoiceBox<Character> colChoiceBox;
	private Button btnExecute;

	private CommandBoardModel model;

	public CommandBoardView() {
		root = new GridPane();
		root.setAlignment(Pos.CENTER);
		root.setVgap(5);
		root.setPadding(new Insets(25, 25, 25, 25));

		Text scenetitle = new Text("Command Board");
		scenetitle.setFont(Font.font(App.HEADER_FONT, App.HEADER_FONT_WEIGHT, App.HEADER_FONT_SIZE));
		root.add(scenetitle, 0, 0);

		Label strategyLabel = new Label("Attack Strategy:");
		root.add(strategyLabel, 0, 1);

		strategyChoiceBox = new ChoiceBox<AttackStrategy>();
		root.add(strategyChoiceBox, 1, 1);

		Label dutyShipLabel = new Label("Duty Ship:");
		root.add(dutyShipLabel, 0, 2);

		shipChoiceBox = new ChoiceBox<NavalShip>();
		shipChoiceBox.setConverter(new StringConverter<NavalShip>() {

			@Override
			public NavalShip fromString(String string) {
				return null;
			}

			@Override
			public String toString(NavalShip ship) {
				return ship.getName() + " - " + ship.getHealth();
			}
		});
		root.add(shipChoiceBox, 1, 2);

		Label targetLabel = new Label("Target:");
		root.add(targetLabel, 0, 3);
		HBox targetField = new HBox(5);
		rowChoiceBox = new ChoiceBox<Integer>();
		colChoiceBox = new ChoiceBox<Character>();
		targetField.getChildren().addAll(rowChoiceBox, colChoiceBox);
		root.add(targetField, 1, 3);

		btnExecute = new Button("Execute");
		root.add(btnExecute, 1, 4);

		strategyChoiceBox.setDisable(disabled);
		shipChoiceBox.setDisable(disabled);
		rowChoiceBox.setDisable(disabled);
		colChoiceBox.setDisable(disabled);
		btnExecute.setDisable(disabled);
	}

	public void update() {
		strategyChoiceBox.getItems().clear();
		strategyChoiceBox.setItems(model.getStrategyOptions());
		strategyChoiceBox.getSelectionModel().select(model.getAttackStrategy());

		shipChoiceBox.getItems().clear();
		shipChoiceBox.setItems(model.getShipOptions());
		shipChoiceBox.getSelectionModel().select(model.getDutyShip());

		rowChoiceBox.setItems(model.getRowOptions());
		rowChoiceBox.getSelectionModel().select(model.getTargetRow());

		colChoiceBox.setItems(model.getColOptions());
		colChoiceBox.getSelectionModel().select(model.getTargetCol());
	}

	public GridPane getRoot() {
		return root;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
		strategyChoiceBox.setDisable(disabled);
		shipChoiceBox.setDisable(disabled);
		rowChoiceBox.setDisable(disabled);
		colChoiceBox.setDisable(disabled);
		btnExecute.setDisable(disabled);
	}

	public Button getBtnExecute() {
		return btnExecute;
	}

	public ChoiceBox<AttackStrategy> getStrategyChoiceBox() {
		return strategyChoiceBox;
	}

	public ChoiceBox<Character> getColChoiceBox() {
		return colChoiceBox;
	}

	public ChoiceBox<Integer> getRowChoiceBox() {
		return rowChoiceBox;
	}

	public ChoiceBox<NavalShip> getShipChoiceBox() {
		return shipChoiceBox;
	}

	public CommandBoardModel getModel() {
		return model;
	}

	public void setModel(CommandBoardModel model) {
		this.model = model;
	}
}
