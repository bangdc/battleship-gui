package docongbang.com.battleship.gui_2.scenes.play.game.views;

import java.util.Hashtable;
import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.CellTrackingAttack;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.utils.Helper;
import docongbang.com.battleship.gui_2.App;
import docongbang.com.battleship.gui_2.scenes.play.command_board.CommandBoardController;
import docongbang.com.battleship.gui_2.scenes.play.command_board.CommandBoardModel;
import docongbang.com.battleship.gui_2.scenes.play.command_board.CommandBoardView;
import docongbang.com.battleship.gui_2.scenes.play.game.models.GameModel;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class GameView {
	private BorderPane root;

	private Text top;
	private HBox center1;
	private VBox right;
	private VBox left;
	private HBox center2;
	private VBox center;

	private Text currentPlayerDisplay;

	private HBox gameMenuButtonsBar;
	private Button btnStart;
	private Button btnPause;
	private Button btnContinue;

	private TilePane player1GridTrackingFleetView;
	private TilePane player1GridTrackingAttackView;

	private TilePane player2GridTrackingFleetView;
	private TilePane player2GridTrackingAttackView;

	private PlayerInfoView player1InfoView;
	private PlayerInfoView player2InfoView;

	private Hashtable<Integer, CellTrackingAttackView> player1TrackingAttackCellViews;
	private Hashtable<Integer, CellTrackingFleetView> player1TrackingFLeetCellViews;

	private Hashtable<Integer, CellTrackingAttackView> player2TrackingAttackCellViews;
	private Hashtable<Integer, CellTrackingFleetView> player2TrackingFLeetCellViews;

	private CommandBoardView commandBoardView;
	private TimerView timerView;
	private NotificationsView notificationsView;

	private Alert commandConfirmationAlert;
	private Alert winnerAnnoucementAlert;

	private GameModel model;

	public GameView() {
		root = new BorderPane();

		top = new Text();
		right = new VBox(5);
		left = new VBox(5);
		center1 = new HBox(5);
		center2 = new HBox(5);
		center = new VBox(5);

		BorderPane.setMargin(top, new Insets(0, 0, 30, 0));
		top.setFont(Font.font(App.HEADER_FONT, App.HEADER_FONT_WEIGHT, App.HEADER_FONT_SIZE));

		BorderPane.setAlignment(top, Pos.CENTER);
		BorderPane.setAlignment(center, Pos.CENTER);
		BorderPane.setAlignment(center1, Pos.CENTER);
		BorderPane.setAlignment(center2, Pos.CENTER);
		BorderPane.setAlignment(left, Pos.CENTER);
		BorderPane.setAlignment(right, Pos.CENTER);
		right.setAlignment(Pos.TOP_CENTER);
		center1.setAlignment(Pos.TOP_CENTER);
		center2.setAlignment(Pos.TOP_CENTER);
		center.setAlignment(Pos.TOP_CENTER);

		player1GridTrackingAttackView = new TilePane();
		player1GridTrackingAttackView.setMinWidth(App.CELL_SIZE * (Game.GRID_WIDTH + 1));
		player1GridTrackingAttackView.setMinHeight(App.CELL_SIZE * (Game.GRID_HEIGHT + 1));
		player1GridTrackingAttackView.setPrefColumns(Game.GRID_WIDTH + 1);
		player1GridTrackingAttackView.setPrefRows(Game.GRID_HEIGHT + 1);

		player1GridTrackingFleetView = new TilePane();
		player1GridTrackingFleetView.setMinWidth(App.CELL_SIZE * (Game.GRID_WIDTH + 1));
		player1GridTrackingFleetView.setMinHeight(App.CELL_SIZE * (Game.GRID_HEIGHT + 1));
		player1GridTrackingFleetView.setPrefColumns(Game.GRID_WIDTH + 1);
		player1GridTrackingFleetView.setPrefRows(Game.GRID_HEIGHT + 1);

		player1TrackingAttackCellViews = new Hashtable<Integer, CellTrackingAttackView>();
		player1TrackingFLeetCellViews = new Hashtable<Integer, CellTrackingFleetView>();

		HBox.setMargin(player1GridTrackingFleetView, new Insets(0, 0, 0, 5));
		center1.getChildren().add(player1GridTrackingAttackView);
		center1.getChildren().add(player1GridTrackingFleetView);
		center.getChildren().add(center1);

		if (Game.DEBUGGABLE) {
			player2GridTrackingAttackView = new TilePane();
			player2GridTrackingAttackView.setMinWidth(App.CELL_SIZE * (Game.GRID_WIDTH + 1));
			player2GridTrackingAttackView.setMinHeight(App.CELL_SIZE * (Game.GRID_HEIGHT + 1));
			player2GridTrackingAttackView.setPrefColumns(Game.GRID_WIDTH + 1);
			player2GridTrackingAttackView.setPrefRows(Game.GRID_HEIGHT + 1);

			player2GridTrackingFleetView = new TilePane();
			player2GridTrackingFleetView.setMinWidth(App.CELL_SIZE * (Game.GRID_WIDTH + 1));
			player2GridTrackingFleetView.setMinHeight(App.CELL_SIZE * (Game.GRID_HEIGHT + 1));
			player2GridTrackingFleetView.setPrefColumns(Game.GRID_WIDTH + 1);
			player2GridTrackingFleetView.setPrefRows(Game.GRID_HEIGHT + 1);

			player2TrackingAttackCellViews = new Hashtable<Integer, CellTrackingAttackView>();
			player2TrackingFLeetCellViews = new Hashtable<Integer, CellTrackingFleetView>();

			HBox.setMargin(player2GridTrackingFleetView, new Insets(0, 0, 0, 5));
			center2.getChildren().add(player2GridTrackingAttackView);
			center2.getChildren().add(player2GridTrackingFleetView);
			center.getChildren().add(center2);
		}

		VBox gameMenu = new VBox(10);
		gameMenu.setPadding(new Insets(25, 25, 25, 25));

		currentPlayerDisplay = new Text();
		currentPlayerDisplay.setFont(Font.font(App.HEADER_FONT, App.HEADER_FONT_WEIGHT, App.HEADER_FONT_SIZE));
		gameMenu.getChildren().add(currentPlayerDisplay);

		gameMenuButtonsBar = new HBox(10);
		btnStart = new Button("Start");
		btnContinue = new Button("Continue");
		btnContinue.setDisable(true);
		btnPause = new Button("Pause");
		btnPause.setDisable(true);
		gameMenuButtonsBar.getChildren().addAll(btnStart, btnPause, btnContinue);
		gameMenu.getChildren().add(gameMenuButtonsBar);

		left.getChildren().add(gameMenu);

		commandBoardView = new CommandBoardView();
		left.getChildren().add(commandBoardView.getRoot());

		timerView = new TimerView();
		left.getChildren().add(timerView.getRoot());

		notificationsView = new NotificationsView();
		left.getChildren().add(notificationsView.getRoot());

		root.setTop(top);
		root.setRight(right);
		root.setLeft(left);
		root.setCenter(center);

		commandConfirmationAlert = new Alert(AlertType.CONFIRMATION);
		commandConfirmationAlert.setTitle("Confirmation Dialog");
		commandConfirmationAlert.setHeaderText("Are you sure?");

		winnerAnnoucementAlert = new Alert(AlertType.INFORMATION);
		winnerAnnoucementAlert.setTitle("Game Over");
		winnerAnnoucementAlert.setContentText("Do you want a new game?");
	}

	public void update() {
		Hashtable<Integer, CellTrackingAttack> player1TrackingAttackCells, player2TrackingAttackCells;
		Hashtable<Integer, CellTrackingFleet> player1TrackingFleetCells, player2TrackingFleetCells;
		player1TrackingAttackCells = model.getGame().getPlayer1().getGridTrackingAttackResult().getCells();
		player1TrackingFleetCells = model.getGame().getPlayer1().getGridTrackingFleet().getCells();
		if (Game.DEBUGGABLE) {
			player2TrackingAttackCells = model.getGame().getPlayer2().getGridTrackingAttackResult().getCells();
			player2TrackingFleetCells = model.getGame().getPlayer2().getGridTrackingFleet().getCells();
		}

		getPlayer1InfoView().update(model.getGame().getPlayer1());
		if (Game.DEBUGGABLE) {
			getPlayer2InfoView().update(model.getGame().getPlayer2());
		}

		for (int col = 0; col < Game.GRID_WIDTH; col++) {
			for (int row = 0; row < Game.GRID_HEIGHT; row++) {
				Integer cellIndex = Integer.valueOf(Helper.calculatePairingNumber(col, row));

				getPlayer1TrackingAttackCellViews().get(cellIndex).update(player1TrackingAttackCells.get(cellIndex));
				getPlayer1TrackingFLeetCellViews().get(cellIndex).update(player1TrackingFleetCells.get(cellIndex));

				if (Game.DEBUGGABLE) {
					getPlayer2TrackingAttackCellViews().get(cellIndex)
							.update(player2TrackingAttackCells.get(cellIndex));
					getPlayer2TrackingFLeetCellViews().get(cellIndex).update(player2TrackingFleetCells.get(cellIndex));
				}
			}
		}
		getCommandBoardView().update();
		commandConfirmationAlert.close();
	}

	public void init() {
		Game game = model.getGame();
		Player player1 = game.getPlayer1();
		Player player2 = game.getPlayer2();

		Hashtable<Integer, CellTrackingAttack> player1TrackingAttackCells, player2TrackingAttackCells;
		Hashtable<Integer, CellTrackingFleet> player1TrackingFleetCells, player2TrackingFleetCells;
		player1TrackingAttackCells = model.getGame().getPlayer1().getGridTrackingAttackResult().getCells();
		player1TrackingFleetCells = model.getGame().getPlayer1().getGridTrackingFleet().getCells();
		if (Game.DEBUGGABLE) {
			player2TrackingAttackCells = model.getGame().getPlayer2().getGridTrackingAttackResult().getCells();
			player2TrackingFleetCells = model.getGame().getPlayer2().getGridTrackingFleet().getCells();
		}

		top.setText(player1.getName() + " VS " + player2.getName());

		player1InfoView = new PlayerInfoView(player1);
		right.getChildren().add(player1InfoView.getRoot());
		HBox.setMargin(player1InfoView.getRoot(), new Insets(0, 0, 0, 20));

		if (Game.DEBUGGABLE) {
			player2InfoView = new PlayerInfoView(player2);
			right.getChildren().add(player2InfoView.getRoot());
			HBox.setMargin(player2InfoView.getRoot(), new Insets(0, 0, 0, 20));
		}

		player1GridTrackingAttackView.getChildren().add(new Label());
		player1GridTrackingFleetView.getChildren().add(new Label());
		if (Game.DEBUGGABLE) {
			player2GridTrackingAttackView.getChildren().add(new Label());
			player2GridTrackingFleetView.getChildren().add(new Label());
		}

		for (int col = 0; col < Game.GRID_WIDTH; col++) {
			player1GridTrackingAttackView.getChildren().add(new Label(String.valueOf((char) ('A' + col))));
			player1GridTrackingFleetView.getChildren().add(new Label(String.valueOf((char) ('A' + col))));
			if (Game.DEBUGGABLE) {
				player2GridTrackingAttackView.getChildren().add(new Label(String.valueOf((char) ('A' + col))));
				player2GridTrackingFleetView.getChildren().add(new Label(String.valueOf((char) ('A' + col))));
			}
		}
		for (int row = 0; row < Game.GRID_HEIGHT; row++) {
			player1GridTrackingAttackView.getChildren().add(new Label(String.valueOf(row + 1)));
			player1GridTrackingFleetView.getChildren().add(new Label(String.valueOf(row + 1)));
			if (Game.DEBUGGABLE) {
				player2GridTrackingAttackView.getChildren().add(new Label(String.valueOf(row + 1)));
				player2GridTrackingFleetView.getChildren().add(new Label(String.valueOf(row + 1)));
			}
			for (int col = 0; col < Game.GRID_WIDTH; col++) {
				Integer cellIndex = Integer.valueOf(Helper.calculatePairingNumber(col, row));

				CellTrackingFleetView player1CellTrackingFleetView = new CellTrackingFleetView(
						player1TrackingFleetCells.get(cellIndex));
				player1TrackingFLeetCellViews.put(cellIndex, player1CellTrackingFleetView);
				player1GridTrackingFleetView.getChildren().add(player1CellTrackingFleetView.getRoot());

				CellTrackingAttackView player1CellTrackingAttackView = new CellTrackingAttackView(
						player1TrackingAttackCells.get(cellIndex));
				player1TrackingAttackCellViews.put(cellIndex, player1CellTrackingAttackView);
				player1GridTrackingAttackView.getChildren().add(player1CellTrackingAttackView.getRoot());

				if (Game.DEBUGGABLE) {
					CellTrackingFleetView player2CellTrackingFleetView = new CellTrackingFleetView(
							player2TrackingFleetCells.get(cellIndex));
					player2TrackingFLeetCellViews.put(cellIndex, player2CellTrackingFleetView);
					player2GridTrackingFleetView.getChildren().add(player2CellTrackingFleetView.getRoot());

					CellTrackingAttackView player2CellTrackingAttackView = new CellTrackingAttackView(
							player2TrackingAttackCells.get(cellIndex));
					player2TrackingAttackCellViews.put(cellIndex, player2CellTrackingAttackView);
					player2GridTrackingAttackView.getChildren().add(player2CellTrackingAttackView.getRoot());
				}
			}
		}

		CommandBoardModel commandBoardModel = new CommandBoardModel(player1);
		new CommandBoardController(commandBoardModel, commandBoardView);
	}

	public Node getRoot() {
		return root;
	}

	public PlayerInfoView getPlayer1InfoView() {
		return player1InfoView;
	}

	public PlayerInfoView getPlayer2InfoView() {
		return player2InfoView;
	}

	public Hashtable<Integer, CellTrackingAttackView> getPlayer1TrackingAttackCellViews() {
		return player1TrackingAttackCellViews;
	}

	public Hashtable<Integer, CellTrackingFleetView> getPlayer1TrackingFLeetCellViews() {
		return player1TrackingFLeetCellViews;
	}

	public Hashtable<Integer, CellTrackingAttackView> getPlayer2TrackingAttackCellViews() {
		return player2TrackingAttackCellViews;
	}

	public Hashtable<Integer, CellTrackingFleetView> getPlayer2TrackingFLeetCellViews() {
		return player2TrackingFLeetCellViews;
	}

	public Button getBtnStart() {
		return btnStart;
	}

	public Button getBtnContinue() {
		return btnContinue;
	}

	public Button getBtnPause() {
		return btnPause;
	}

	public CommandBoardView getCommandBoardView() {
		return commandBoardView;
	}

	public TimerView getTimerView() {
		return timerView;
	}

	public void setModel(GameModel model) {
		this.model = model;
	}

	public GameModel getModel() {
		return model;
	}

	public NotificationsView getNotificationsView() {
		return notificationsView;
	}

	public Alert getCommandConfirmationAlert() {
		return commandConfirmationAlert;
	}

	public Alert getWinnerAnnoucementAlert() {
		return winnerAnnoucementAlert;
	}

	public Text getCurrentPlayerDisplay() {
		return currentPlayerDisplay;
	}
}
