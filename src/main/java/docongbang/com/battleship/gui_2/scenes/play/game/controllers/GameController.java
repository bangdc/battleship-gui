package docongbang.com.battleship.gui_2.scenes.play.game.controllers;

import java.util.Optional;
import java.util.function.Consumer;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.CellBomb;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.player.PlayerComputer;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.utils.AttackStrategy;
import docongbang.com.battleship.gui_2.App;
import docongbang.com.battleship.gui_2.scenes.play.game.models.GameModel;
import docongbang.com.battleship.gui_2.scenes.play.game.views.GameView;
import docongbang.com.battleship.gui_2.scenes.play.upgrade_ship_dialog.UpgradeShipDialogController;
import docongbang.com.battleship.gui_2.scenes.play.upgrade_ship_dialog.UpgradeShipDialogModel;
import docongbang.com.battleship.gui_2.scenes.play.upgrade_ship_dialog.UpgradeShipDialogView;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.util.Pair;

public class GameController extends Task<Void> implements EventHandler<MouseEvent> {
	private GameModel model;
	private GameView view;

	private Thread timerThread;
	private Thread computerTurnThread;

	private Thread gameManagerThread;
	private Thread notificationManagerThread;

	public GameController(final GameModel model, final GameView view) {
		this.model = model;
		this.view = view;

		view.setModel(model);
		view.init();

		view.getBtnStart().setOnMouseClicked(this);
		view.getBtnPause().setOnMouseClicked(this);
		view.getBtnContinue().setOnMouseClicked(this);
		view.getCommandBoardView().getBtnExecute().setOnMouseClicked(this);
		view.getTimerView().startTimerTask(model.getTimerSharedData());

		gameManagerThread = new Thread(this, "Thread-game-manager");
		gameManagerThread.setDaemon(true);

		notificationManagerThread = new Thread(new TaskUpdateNotification(model, view), "Thread-notification-manager");
		notificationManagerThread.setDaemon(true);
	}

	public void handle(MouseEvent event) {
		// Start
		if (event.getSource() == view.getBtnStart()) {
			gameManagerThread.start();
			notificationManagerThread.start();

			view.getBtnStart().setDisable(true);
			view.getBtnPause().setDisable(false);
			view.getBtnContinue().setDisable(true);
		}
		// Pause
		if (event.getSource() == view.getBtnPause()) {
			model.getGamePausedSharedData().setData(true);
			view.getBtnPause().setDisable(true);
			view.getBtnContinue().setDisable(false);
			view.getCommandBoardView().setDisabled(true);
		}
		// Continue
		if (event.getSource() == view.getBtnContinue()) {
			view.getBtnPause().setDisable(false);
			view.getBtnContinue().setDisable(true);
			if (model.getGame().getCurrentPlayer() instanceof PlayerComputer) {
				view.getCommandBoardView().setDisabled(true);
			} else {
				view.getCommandBoardView().setDisabled(false);
			}
			synchronized (model.getGamePausedSharedData()) {
				model.getGamePausedSharedData().setData(false);
				model.getGamePausedSharedData().notifyAll();
			}
		}
		// command
		if (event.getSource() == view.getCommandBoardView().getBtnExecute()) {
			AttackStrategy strategy = view.getCommandBoardView().getModel().getAttackStrategy();
			CellTrackingFleet target = view.getCommandBoardView().getModel().getTarget();
			NavalShip dutyShip = view.getCommandBoardView().getModel().getDutyShip();
			StringBuilder confirmationContent = new StringBuilder();
			confirmationContent.append("You are going to attack ");
			confirmationContent.append(" by ship " + dutyShip.getName());
			confirmationContent.append(" at target " + target.toString());
			confirmationContent.append(" with strategy " + strategy.toString());
			view.getCommandConfirmationAlert().setContentText(confirmationContent.toString());
			Optional<ButtonType> result = view.getCommandConfirmationAlert().showAndWait();
			if (result.get() == ButtonType.OK) {
				model.getTurnAttackStrategySharedData().setData(strategy);
				model.getTurnAttackTargetSharedData().setData(target);
				model.getTurnDutyShipSharedData().setData(dutyShip);
				timerThread.interrupt();
				model.getTurnMissedSharedData().setData(false);
				Platform.runLater(new Runnable() {
					public void run() {
						App.SOUND_MISSLE_LAUNCH.play();
					}
				});
			}
		}
	}

	@Override
	protected Void call() throws Exception {
		int turnNumber = 1;
		Player player1 = model.getGame().getPlayer1();
		Player player2 = model.getGame().getPlayer2();

		model.getGame().setCurrentPlayer(player1);
		model.getGame().getCurrentPlayer().setNumberOfTurnsRemaining(1);

		while (true) {
			final Player currentPlayer = model.getGame().getCurrentPlayer();
			final Player opponent = currentPlayer.getOpponent();

			timerThread = new Thread(new TaskTurnTimer(model));
			timerThread.setDaemon(true);

			Platform.runLater(new Runnable() {

				public void run() {
					view.getCurrentPlayerDisplay().setText(
							currentPlayer.getName() + " has " + currentPlayer.getNumberOfTurnsRemaining() + " turn(s)");
				}
			});

			// Before attacking
			if (currentPlayer == player1) {
				view.getCommandBoardView().setDisabled(false);
			} else {
				view.getCommandBoardView().setDisabled(true);
			}
			model.getNotifications().writeMessage("TURN #" + turnNumber + "-" + currentPlayer.getName());
			if (currentPlayer == player2) {
				computerTurnThread = new Thread(new TaskTurnComputer(model, this));
				computerTurnThread.setDaemon(true);
				computerTurnThread.start();
			}
			timerThread.start();
			timerThread.join();

			// Attacking
			currentPlayer.setNumberOfTurnsPlayed(currentPlayer.getNumberOfTurnsPlayed() + 1);
			if (model.getTurnMissedSharedData().getData()) {
				model.getNotifications().writeMessage("\t" + currentPlayer.getName() + " MISSED!");
				currentPlayer.fail();
			} else {
				NavalShip dutyShip = model.getTurnDutyShipSharedData().getData();
				AttackStrategy attackStrategy = model.getTurnAttackStrategySharedData().getData();
				CellTrackingFleet target = model.getTurnAttackTargetSharedData().getData();
				if (dutyShip == null) {
					dutyShip = currentPlayer.getMostPowerfulShip();
				}
				if (attackStrategy == null) {
					attackStrategy = currentPlayer.getAttackStrategyRandomly();
				}
				if (target == null) {
					target = currentPlayer.getTargetRandomly();
				}
				// ATTACK
				model.getNotifications()
						.writeMessage("\t" + currentPlayer.getName() + " is attacking " + opponent.getName());
				model.getNotifications()
						.writeMessage("\t" + dutyShip.getName() + "-" + target.toString() + "-" + attackStrategy);
				boolean causedCivilShipDestroyed = opponent.getSailingShip().getHealth() > 0;
				boolean causedBombExploded = !((opponent == player1) ? CellBomb.wasBombPlayer1Triggered()
						: CellBomb.wasBombPlayer2Triggered());
				if (currentPlayer.attack(dutyShip, target, attackStrategy)) {
					model.getNotifications().writeMessage("\t" + currentPlayer.getName() + " SUCCEEDED!");
					currentPlayer.setNumberOfConsecutiveFailures(0);
					Platform.runLater(new Runnable() {
						public void run() {
							App.SOUND_SUCCEEDED.play();
						}
					});
				} else {
					model.getNotifications().writeMessage("\t" + currentPlayer.getName() + " FAILED!");
					currentPlayer.fail();
					Platform.runLater(new Runnable() {
						public void run() {
							App.SOUND_FAILED.play();
						}
					});
				}
				causedCivilShipDestroyed &= opponent.getSailingShip().getHealth() == 0;
				if (causedCivilShipDestroyed) {
					model.getNotifications().writeMessage("\t" + currentPlayer.getName() + " DESTROYED CIVIL SHIP!");
				}
				causedBombExploded &= ((opponent == player1) ? CellBomb.wasBombPlayer1Triggered()
						: CellBomb.wasBombPlayer2Triggered());
				if (causedBombExploded) {
					Platform.runLater(new Runnable() {
						public void run() {
							App.SOUND_BOMB_EXPLODED.play();
						}
					});
				}
			}

			// After attacking
			// update model
			model.getGame().updateAfterEachTurn();
			// update view
			Platform.runLater(new Runnable() {
				public void run() {
					view.update();
				}
			});

			// Check if game is over.
			if (player1.lostAllShips()) {
				model.getGame().finish(player2, player1);
				break;
			}
			if (player2.lostAllShips()) {
				model.getGame().finish(player1, player2);
				break;
			}

			if (currentPlayer.getNumberOfConsecutiveFailures() > 0 && currentPlayer.getNumberOfConsecutiveFailures()
					% Game.NUMBER_CONSECUTIVE_FAILURES_TO_UNLOCK_ROCKET == 0) {
				currentPlayer.unlockSubmarineWithRocket();
				model.getNotifications()
						.writeMessage("\t" + currentPlayer.getName() + " unlocked the rocket of submarine!");
			}

			// If current player exceeds the number of consecutive failures,
			// the opponent can improve his/her ship.
			if (currentPlayer.getNumberOfConsecutiveFailures() > 0 && currentPlayer.getNumberOfConsecutiveFailures()
					% Game.NUMBER_CONSECUTIVE_FAILURES_TO_UPGRADE_SHIP == 0) {
				if (opponent instanceof PlayerComputer) {
					opponent.upgradeShipRandomly();
					Platform.runLater(new Runnable() {
						public void run() {
							view.update();
						}
					});
				} else {
					model.getGamePausedSharedData().setData(true);
					Platform.runLater(new Runnable() {

						public void run() {
							UpgradeShipDialogView upgradeShipDialogView = new UpgradeShipDialogView();
							UpgradeShipDialogModel upgradeShipDialogModel = new UpgradeShipDialogModel(opponent);
							new UpgradeShipDialogController(upgradeShipDialogModel, upgradeShipDialogView);

							Optional<Pair<NavalShip, CellTrackingFleet>> result = upgradeShipDialogView.getDialog()
									.showAndWait();
							result.ifPresent(new Consumer<Pair<NavalShip, CellTrackingFleet>>() {
								public void accept(Pair<NavalShip, CellTrackingFleet> shipAndCellToUpgrade) {
									opponent.upgradeShip(shipAndCellToUpgrade.getKey(),
											shipAndCellToUpgrade.getValue());
									Platform.runLater(new Runnable() {
										public void run() {
											view.update();
										}
									});
								}
							});
							synchronized (model.getGamePausedSharedData()) {
								model.getGamePausedSharedData().setData(false);
								model.getGamePausedSharedData().notifyAll();
							}
						}
					});
				}
				model.getNotifications().writeMessage("\t" + opponent.getName() + " upgraded a ship");
			}
			if (currentPlayer.getNumberOfTurnsRemaining() <= 0) {
				currentPlayer.setNumberOfTurnsRemaining(0);
				model.getGame().setCurrentPlayer(opponent);
			} else {
				opponent.setNumberOfTurnsRemaining(0);
			}
			turnNumber++;
			model.getTurnAttackStrategySharedData().setData(null);
			model.getTurnAttackTargetSharedData().setData(null);
			model.getTurnDutyShipSharedData().setData(null);
			view.getCommandBoardView().setDisabled(true);
			Thread.sleep(1500);
		}
		Platform.runLater(new Runnable() {

			public void run() {
				view.getWinnerAnnoucementAlert()
						.setHeaderText("Game is over! " + model.getGame().getWinner().getName() + " won!");
				view.getWinnerAnnoucementAlert().showAndWait();
				// Reset Game
				model.getGame().reset();
				// Ask for a new game
				App.getPrimaryStage().setScene(App.getSceneNewGame());
				App.getPrimaryStage().setMaximized(true);
			}
		});
		return null;
	}

	public Thread getComputerTurnThread() {
		return computerTurnThread;
	}

	public Thread getTimerThread() {
		return timerThread;
	}

	public Thread getGameManagerThread() {
		return gameManagerThread;
	}
}
