package docongbang.com.battleship.gui_2.scenes.play.game.models;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.utils.AttackStrategy;
import docongbang.com.battleship.gui_2.util.MailBox;
import docongbang.com.battleship.gui_2.util.SharedData;

public class GameModel {
	private Game game;

	private volatile SharedData<Double> timerSharedData;
	private volatile SharedData<Boolean> gamePausedSharedData;
	private volatile SharedData<Boolean> turnMissedSharedData;

	private volatile SharedData<NavalShip> turnDutyShipSharedData;
	private volatile SharedData<AttackStrategy> turnAttackStrategySharedData;
	private volatile SharedData<CellTrackingFleet> turnAttackTargetSharedData;

	private volatile MailBox<String> notifications;

	public GameModel(Game game) {
		this.game = game;
		timerSharedData = new SharedData<Double>();
		gamePausedSharedData = new SharedData<Boolean>();
		gamePausedSharedData.setData(false);
		turnMissedSharedData = new SharedData<Boolean>();
		turnMissedSharedData.setData(false);
		turnAttackStrategySharedData = new SharedData<AttackStrategy>();
		turnAttackStrategySharedData.setData(AttackStrategy.CLASSIC);
		turnAttackTargetSharedData = new SharedData<CellTrackingFleet>();
		turnAttackTargetSharedData.setData(null);
		turnDutyShipSharedData = new SharedData<NavalShip>();
		turnDutyShipSharedData.setData(null);
		notifications = new MailBox<String>(5);
	}

	public Game getGame() {
		return game;
	}

	public SharedData<Double> getTimerSharedData() {
		return timerSharedData;
	}

	public SharedData<Boolean> getGamePausedSharedData() {
		return gamePausedSharedData;
	}

	public SharedData<Boolean> getTurnMissedSharedData() {
		return turnMissedSharedData;
	}

	public SharedData<NavalShip> getTurnDutyShipSharedData() {
		return turnDutyShipSharedData;
	}

	public SharedData<AttackStrategy> getTurnAttackStrategySharedData() {
		return turnAttackStrategySharedData;
	}

	public SharedData<CellTrackingFleet> getTurnAttackTargetSharedData() {
		return turnAttackTargetSharedData;
	}

	public MailBox<String> getNotifications() {
		return notifications;
	}
}
