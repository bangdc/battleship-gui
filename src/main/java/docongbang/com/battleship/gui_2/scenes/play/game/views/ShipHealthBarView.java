package docongbang.com.battleship.gui_2.scenes.play.game.views;

import javafx.scene.control.ProgressBar;

public class ShipHealthBarView {
	private double maxHealth;
	private double currentHealth = 0;

	private ProgressBar healthProgressBar;

	public ShipHealthBarView(double maxHealth, double currentHealth) {
		this.maxHealth = maxHealth;
		healthProgressBar = new ProgressBar();
		update(currentHealth);
	}

	public ProgressBar getHealthProgressBar() {
		return healthProgressBar;
	}

	public void update(double currentHealth) {
		this.currentHealth = currentHealth;
		healthProgressBar.setProgress(this.currentHealth / maxHealth);
	}
}
