package docongbang.com.battleship.gui_2.scenes.play.game.controllers;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.gui_2.scenes.play.game.models.GameModel;

public class TaskTurnTimer implements Runnable {
	private GameModel gameModel;

	public TaskTurnTimer(GameModel gameModel) {
		this.gameModel = gameModel;
	}

	public void run() {
		gameModel.getTurnMissedSharedData().setData(true);
		for (int i = 100; i >= 0; i--) {
			synchronized (gameModel.getGamePausedSharedData()) {
				while (gameModel.getGamePausedSharedData().getData()) {
					try {
						gameModel.getGamePausedSharedData().wait();
					} catch (InterruptedException e) {
						break;
					}
				}
			}
			gameModel.getTimerSharedData().setData((double) (i));
			try {
				Thread.sleep(Game.TIME_CONSTRAINT_EACH_TURN / 100);
			} catch (InterruptedException e) {
				gameModel.getTimerSharedData().setData((double) 0);
				// If timerThread is interrupted, the current
				// player don't miss this turn.
				gameModel.getTurnMissedSharedData().setData(false);
				break;
			}
		}
	}
}
