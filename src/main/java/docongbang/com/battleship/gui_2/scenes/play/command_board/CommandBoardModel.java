package docongbang.com.battleship.gui_2.scenes.play.command_board;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.utils.AttackStrategy;
import docongbang.com.battleship.api.utils.Helper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CommandBoardModel {
	private NavalShip dutyShip;
	private AttackStrategy attackStrategy;
	private Character targetCol;
	private Integer targetRow;

	private ObservableList<Integer> rowOptions;
	private ObservableList<Character> colOptions;

	private Player player;

	public CommandBoardModel(Player player) {
		this.player = player;

		rowOptions = FXCollections.observableArrayList();
		for (int i = 0; i < Game.GRID_HEIGHT; i++) {
			rowOptions.add(i + 1);
		}

		colOptions = FXCollections.observableArrayList();
		for (int i = 0; i < Game.GRID_WIDTH; i++) {
			colOptions.add(Character.valueOf((char) (i + (int) 'A')));
		}

		dutyShip = player.getShips().get(0);
		attackStrategy = AttackStrategy.CLASSIC;
		targetCol = colOptions.get(0);
		targetRow = rowOptions.get(0);
	}

	public AttackStrategy getAttackStrategy() {
		return attackStrategy;
	}

	public void setAttackStrategy(AttackStrategy attackStrategy) {
		this.attackStrategy = attackStrategy;
	}

	public NavalShip getDutyShip() {
		return dutyShip;
	}

	public void setDutyShip(NavalShip dutyShip) {
		this.dutyShip = dutyShip;
	}

	public CellTrackingFleet getTarget() {
		final int row = targetRow.intValue() - 1;
		final int col = (int) targetCol.charValue() - (int) 'A';
		final Integer cellIndex = Integer.valueOf(Helper.calculatePairingNumber(col, row));
		return player.getOpponent().getGridTrackingFleet().getCells().get(cellIndex);
	}

	public Player getPlayer() {
		return player;
	}

	public ObservableList<Character> getColOptions() {
		return colOptions;
	}

	public ObservableList<AttackStrategy> getStrategyOptions() {
		if (player.ableToUseRocket()) {
			return FXCollections.observableArrayList(AttackStrategy.values());
		} else {
			if (attackStrategy == null || attackStrategy == AttackStrategy.ILLUMINATING_ROCKET) {
				attackStrategy = AttackStrategy.CLASSIC;
			}
			return FXCollections.observableArrayList(AttackStrategy.CLASSIC, AttackStrategy.CROSS);
		}
	}

	public ObservableList<NavalShip> getShipOptions() {
		ObservableList<NavalShip> shipOptions = FXCollections.observableArrayList();
		if (attackStrategy == AttackStrategy.ILLUMINATING_ROCKET) {
			if (player.getSubmarine().getHealth() > 0) {
				shipOptions.add(player.getSubmarine());
				dutyShip = player.getSubmarine();
			}
		} else {
			for (NavalShip ship : player.getShips()) {
				if (ship.getHealth() > 0) {
					shipOptions.add(ship);
				}
			}
		}
		return shipOptions;
	}

	public ObservableList<Integer> getRowOptions() {
		return rowOptions;
	}

	public Character getTargetCol() {
		return targetCol;
	}

	public void setTargetCol(Character targetCol) {
		this.targetCol = targetCol;
	}

	public Integer getTargetRow() {
		return targetRow;
	}

	public void setTargetRow(Integer targetRow) {
		this.targetRow = targetRow;
	}
}
