package docongbang.com.battleship.gui_2.scenes.new_game;

import docongbang.com.battleship.api.utils.DifficultyLevel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class NewGameModel {
	private String playerName;
	private DifficultyLevel gameDifficulty;
	private ObservableList<DifficultyLevel> difficultyOptions;

	/**
	 * Create new model
	 */
	public NewGameModel() {
		difficultyOptions = FXCollections.observableArrayList(DifficultyLevel.values());
		setGameDifficulty(DifficultyLevel.EASY);
	}

	/**
	 * Get player name
	 * 
	 * @return
	 */
	public String getPlayerName() {
		return playerName;
	}

	/**
	 * Set the player name
	 * 
	 * @param playerName
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	/**
	 * Get the difficulty of game
	 * 
	 * @return
	 */
	public DifficultyLevel getGameDifficulty() {
		return gameDifficulty;
	}

	/**
	 * Set the difficulty of game
	 * 
	 * @param gameDifficulty
	 */
	public void setGameDifficulty(DifficultyLevel gameDifficulty) {
		this.gameDifficulty = gameDifficulty;
	}

	/**
	 * Get the different options for difficulty
	 * 
	 * @return
	 */
	public ObservableList<DifficultyLevel> getDifficultyOptions() {
		return difficultyOptions;
	}

}
