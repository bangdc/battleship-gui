package docongbang.com.battleship.gui_2.scenes.play.game.views;

import docongbang.com.battleship.api.grid.cell.CellTrackingAttack;
import docongbang.com.battleship.gui_2.App;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class CellTrackingAttackView {
	private VBox root;
	private HBox main;
	private ImageView ivAttackResult;
	private ImageView ivVisibility;

	public CellTrackingAttackView(CellTrackingAttack cell) {
		root = new VBox();
		main = new HBox(2);

		ivAttackResult = new ImageView();
		ivAttackResult.setFitHeight(App.IMAGE_SHIP_SIZE);
		ivAttackResult.setPreserveRatio(true);

		ivVisibility = new ImageView();
		ivVisibility.setFitHeight(App.IMAGE_SHIP_SIZE);
		ivVisibility.setPreserveRatio(true);

		main.getChildren().addAll(ivAttackResult, ivVisibility);
		main.setAlignment(Pos.CENTER);

		root.getChildren().add(main);
		root.setAlignment(Pos.CENTER);
		root.setBorder(new Border(
				new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		root.setPrefHeight(App.CELL_SIZE);
		root.setPrefWidth(App.CELL_SIZE);

		update(cell);
	}

	public void update(CellTrackingAttack cell) {
		if (cell.wasAttacked()) {
			if (cell.doesHaveShip()) {
				ivAttackResult.setImage(App.IMAGE_ATTACK_SUCCESS);
			} else {
				ivAttackResult.setImage(App.IMAGE_ATTACK_FAIL);
			}
		}
		if (cell.isVisible()) {
			if (cell.doesHaveShip()) {
				ivVisibility.setImage(App.IMAGE_ATTACK_HAS_SHIP);
			} else {
				ivVisibility.setImage(App.IMAGE_ATTACK_NO_SHIP);
			}
		}
	}

	public VBox getRoot() {
		return root;
	}
}
