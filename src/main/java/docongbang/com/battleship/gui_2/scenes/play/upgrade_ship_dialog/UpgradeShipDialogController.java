package docongbang.com.battleship.gui_2.scenes.play.upgrade_ship_dialog;

import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ButtonType;
import javafx.util.Callback;
import javafx.util.Pair;

public class UpgradeShipDialogController
		implements ChangeListener<Object>, Callback<ButtonType, Pair<NavalShip, CellTrackingFleet>> {
	private UpgradeShipDialogModel model;
	private UpgradeShipDialogView view;

	public UpgradeShipDialogController(UpgradeShipDialogModel model, UpgradeShipDialogView view) {
		this.model = model;
		this.view = view;

		view.setModel(model);
		view.update();

		view.getCellChoiceBox().getSelectionModel().selectedItemProperty().addListener(this);
		view.getShipChoiceBox().getSelectionModel().selectedItemProperty().addListener(this);
		view.getDialog().setResultConverter(this);
	}

	public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
		if (newValue instanceof CellTrackingFleet) {
			CellTrackingFleet cell = (CellTrackingFleet) newValue;
			if (cell != model.getCell()) {
				model.setCell((CellTrackingFleet) newValue);
				view.update();
			}
			return;
		}

		if (newValue instanceof NavalShip) {
			NavalShip ship = (NavalShip) newValue;
			if (ship != model.getShip()) {
				model.setShip(ship);
				view.update();
			}
			return;
		}
	}

	public Pair<NavalShip, CellTrackingFleet> call(ButtonType dialogButton) {
		if (dialogButton == ButtonType.OK) {
			return new Pair<NavalShip, CellTrackingFleet>(model.getShip(), model.getCell());
		}
		return null;
	}
}
