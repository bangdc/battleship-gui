package docongbang.com.battleship.gui_2.scenes.play.game.views;

import docongbang.com.battleship.gui_2.App;
import javafx.geometry.Insets;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class NotificationsView {
	private VBox root;
	private ListView<String> notifications;

	public NotificationsView() {
		root = new VBox(10);
		root.setPadding(new Insets(25, 25, 25, 25));

		Text title = new Text("Notifications");
		title.setFont(Font.font(App.HEADER_FONT, App.HEADER_FONT_WEIGHT, App.HEADER_FONT_SIZE));

		notifications = new ListView<String>();
		root.getChildren().addAll(title, notifications);
	}

	public VBox getRoot() {
		return root;
	}

	public ListView<String> getNotifications() {
		return notifications;
	}
}
