package docongbang.com.battleship.gui_2.scenes.new_game;

import docongbang.com.battleship.api.utils.DifficultyLevel;
import docongbang.com.battleship.gui_2.App;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

public class NewGameController implements EventHandler<MouseEvent>, ChangeListener<Object> {
	private NewGameModel model;
	private NewGameView view;

	/**
	 * Create new controller. Binding model to view. Update view and binding
	 * elements of view to some listeners
	 * 
	 * @param model
	 * @param view
	 */
	public NewGameController(NewGameModel model, NewGameView view) {
		this.model = model;
		this.view = view;

		view.setModel(model);
		view.update();

		view.getBtnNewGame().setOnMouseClicked(this);
		view.getUserTextField().textProperty().addListener(this);
		view.getGameDifficultyChoice().getSelectionModel().selectedItemProperty().addListener(this);
	}

	public void handle(MouseEvent event) {
		Button button = (Button) event.getSource();
		if (button == view.getBtnNewGame()) {
			App.setGame(model.getPlayerName(), model.getGameDifficulty());
			App.getPrimaryStage().setScene(App.getSceneShipArrangement());
		}
	}

	public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
		if (newValue instanceof DifficultyLevel) {
			model.setGameDifficulty((DifficultyLevel) newValue);
			return;
		}
		if (newValue instanceof String) {
			model.setPlayerName((String) newValue);
			return;
		}
	}

}
