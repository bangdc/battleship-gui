package docongbang.com.battleship.gui_2.scenes.new_game;

import docongbang.com.battleship.api.utils.DifficultyLevel;
import docongbang.com.battleship.gui_2.App;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class NewGameView {
	private GridPane root;
	private Text scenetitle;
	private TextField userTextField;
	private ChoiceBox<DifficultyLevel> gameDifficultyChoice;
	private Button btnNewGame;
	private NewGameModel model;

	/**
	 * Create new view
	 */
	public NewGameView() {
		root = new GridPane();
		root.setAlignment(Pos.CENTER);
		root.setHgap(10);
		root.setVgap(10);
		root.setPadding(new Insets(25, 25, 25, 25));

		scenetitle = new Text("Welcome to Battleship");
		scenetitle.setFont(Font.font(App.HEADER_FONT, App.HEADER_FONT_WEIGHT, App.HEADER_FONT_SIZE));
		root.add(scenetitle, 0, 0, 2, 1);

		Label userName = new Label("Your name:");
		root.add(userName, 0, 1);

		userTextField = new TextField();
		root.add(userTextField, 1, 1);

		Label gameDifficultyLabel = new Label("Difficulty:");
		root.add(gameDifficultyLabel, 0, 2);

		gameDifficultyChoice = new ChoiceBox<DifficultyLevel>();
		root.add(gameDifficultyChoice, 1, 2);

		btnNewGame = new Button("New Game");
		root.add(btnNewGame, 1, 4);
	}

	/**
	 * Update view
	 */
	public void update() {
		gameDifficultyChoice.setItems(model.getDifficultyOptions());
		gameDifficultyChoice.getSelectionModel().select(model.getGameDifficulty());
	}

	/**
	 * Get the root element of view
	 * 
	 * @return
	 */
	public GridPane getRoot() {
		return root;
	}

	/**
	 * TextField for player name
	 * 
	 * @return
	 */
	public TextField getUserTextField() {
		return userTextField;
	}

	/**
	 * ChoiceBox for difficulty
	 * 
	 * @return
	 */
	public ChoiceBox<DifficultyLevel> getGameDifficultyChoice() {
		return gameDifficultyChoice;
	}

	/**
	 * New game submit button
	 * 
	 * @return
	 */
	public Button getBtnNewGame() {
		return btnNewGame;
	}

	/**
	 * Set Model
	 * 
	 * @param model
	 */
	public void setModel(NewGameModel model) {
		this.model = model;
	}

	/**
	 * Get Model
	 * 
	 * @return
	 */
	public NewGameModel getModel() {
		return model;
	}
}
