package docongbang.com.battleship.gui_2.scenes.ship_arrangement;

import java.util.List;

import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.utils.ShipOrientation;
import docongbang.com.battleship.gui_2.App;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

public class ShipArrangementController implements EventHandler<MouseEvent> {
	private ShipArrangementModel model;
	private ShipArrangementView view;

	public ShipArrangementController(final ShipArrangementModel model, final ShipArrangementView view) {
		this.model = model;
		this.view = view;

		view.setModel(model);
		view.init();

		view.getBtnReady().setOnMouseClicked(this);

		List<NavalShip> ships = model.getPlayer().getShips();
		for (final NavalShip navalShip : ships) {
			view.getShipClearButtons().get(navalShip).setOnMouseClicked(this);
			view.getShipUpdateButtons().get(navalShip).setOnMouseClicked(this);
			view.getShipOriginColChoiceBoxes().get(navalShip).getSelectionModel().selectedItemProperty()
					.addListener(new ChangeListener<Character>() {

						public void changed(ObservableValue<? extends Character> observable, Character oldValue,
								Character newValue) {
							if (navalShip.getOrigin() == null
									|| newValue.charValue() != navalShip.getOrigin().getCol()) {
								view.getShipUpdateButtons().get(navalShip).setDisable(false);
							} else {
								view.getShipUpdateButtons().get(navalShip).setDisable(true);
							}
						}
					});
			view.getShipOriginRowChoiceBoxes().get(navalShip).getSelectionModel().selectedItemProperty()
					.addListener(new ChangeListener<Integer>() {

						public void changed(ObservableValue<? extends Integer> observable, Integer oldValue,
								Integer newValue) {
							if (navalShip.getOrigin() == null
									|| newValue.intValue() != navalShip.getOrigin().getRow()) {
								view.getShipUpdateButtons().get(navalShip).setDisable(false);
							} else {
								view.getShipUpdateButtons().get(navalShip).setDisable(true);
							}
						}
					});
			view.getShipOrientationChoiceBoxes().get(navalShip).getSelectionModel().selectedItemProperty()
					.addListener(new ChangeListener<ShipOrientation>() {

						public void changed(ObservableValue<? extends ShipOrientation> observable,
								ShipOrientation oldValue, ShipOrientation newValue) {
							if (navalShip.getOrientation() == null || newValue != navalShip.getOrientation()) {
								view.getShipUpdateButtons().get(navalShip).setDisable(false);
							} else {
								view.getShipUpdateButtons().get(navalShip).setDisable(true);
							}
						}
					});
		}
	}

	public void handle(MouseEvent event) {
		Button button = (Button) event.getSource();

		if (button == view.getBtnReady()) {
			App.getPrimaryStage().setScene(App.getScenePlay());
			return;
		}

		List<NavalShip> ships = model.getPlayer().getShips();
		for (NavalShip navalShip : ships) {
			if (button == view.getShipClearButtons().get(navalShip)) {
				navalShip.resetPosition();
				view.update();
				return;
			}
			if (button == view.getShipUpdateButtons().get(navalShip)) {
				char col = view.getShipOriginColChoiceBoxes().get(navalShip).getValue().charValue();
				int row = view.getShipOriginRowChoiceBoxes().get(navalShip).getValue().intValue();
				ShipOrientation orientation = view.getShipOrientationChoiceBoxes().get(navalShip).getValue();

				if (!model.getPlayer().getGridTrackingFleet().addShip(navalShip, col, row, orientation)) {
					view.getAlert().setTitle("Position Error");
					view.getAlert().setHeaderText("The position is invalid or already acquired by other.");
					view.getAlert().setContentText("Please try again!");
					view.getAlert().showAndWait();
				}
				view.update();
				return;
			}
		}
	}
}
