package docongbang.com.battleship.gui_2.scenes.play.upgrade_ship_dialog;

import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import javafx.util.StringConverter;

public class UpgradeShipDialogView {
	private Dialog<Pair<NavalShip, CellTrackingFleet>> dialog;
	private GridPane root;
	private ChoiceBox<NavalShip> shipChoiceBox;
	private ChoiceBox<CellTrackingFleet> cellChoiceBox;

	private UpgradeShipDialogModel model;

	public UpgradeShipDialogView() {
		root = new GridPane();
		root.setAlignment(Pos.CENTER);
		root.setHgap(10);
		root.setVgap(10);

		Label shipLabel = new Label("Select Ship:");
		root.add(shipLabel, 0, 0);

		shipChoiceBox = new ChoiceBox<NavalShip>();
		shipChoiceBox.setConverter(new StringConverter<NavalShip>() {

			@Override
			public NavalShip fromString(String string) {
				return null;
			}

			@Override
			public String toString(NavalShip ship) {
				return ship.getName() + " - " + ship.getHealth();
			}
		});
		root.add(shipChoiceBox, 1, 0);

		Label cellLabel = new Label("Select Cell:");
		root.add(cellLabel, 0, 1);

		cellChoiceBox = new ChoiceBox<CellTrackingFleet>();
		cellChoiceBox.setConverter(new StringConverter<CellTrackingFleet>() {

			@Override
			public CellTrackingFleet fromString(String string) {
				return null;
			}

			@Override
			public String toString(CellTrackingFleet cell) {
				return cell.toString();
			}
		});
		root.add(cellChoiceBox, 1, 1);

		dialog = new Dialog<Pair<NavalShip, CellTrackingFleet>>();
		dialog.setTitle("Upgrade a ship");
		dialog.setHeaderText("Select a ship to upgrade");
		dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
		dialog.getDialogPane().setContent(root);
	}

	public UpgradeShipDialogModel getModel() {
		return model;
	}

	public void setModel(UpgradeShipDialogModel model) {
		this.model = model;
	}

	public void update() {
		shipChoiceBox.setItems(model.getShipOptions());
		shipChoiceBox.getSelectionModel().select(model.getShip());

		cellChoiceBox.setItems(model.getCellOptions());
		cellChoiceBox.getSelectionModel().select(model.getCell());
	}

	public Dialog<Pair<NavalShip, CellTrackingFleet>> getDialog() {
		return dialog;
	}

	public ChoiceBox<CellTrackingFleet> getCellChoiceBox() {
		return cellChoiceBox;
	}

	public ChoiceBox<NavalShip> getShipChoiceBox() {
		return shipChoiceBox;
	}
}
